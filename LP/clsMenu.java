package LP;

import java.util.ArrayList;

import COMUN.clsConstantes;
import COMUN.itfProperty;
import LN.clsGestorLN;


/**
* Clase que implementa el menu principal de la aplicacion, permitiendo acceder a
* diferentes funcionalidades.
* Utiliza la clase {@link clsGestorLN} como nexo de union entre las diferentes 
* capas del programa.
* 
* @author Samuel.Aranda
* @version 3.0
*/
public class clsMenu 
{

    /**
    * Objeto de la clase {@link clsGestorLN} utilizado para gestionar y 
    * manipular los datos de los usuarios y vehículos.
    */
    private clsGestorLN obj_gestorLN; // nexo de union entre capas

    /**
    * Constructor de la clase `clsMenu` que inicializa el objeto `obj_gestorLN`.
    */
    public clsMenu() 
    {
        obj_gestorLN = new clsGestorLN();
    }

    /**
    * Metodo principal que muestra el menu inicial de la aplicacion y redirige a
    * los submenus segun la opcion seleccionada.
    */
    public void vo_menu_inicio() 
    {
        char op = ' ';
        do 
        {
            System.out.println("********** Bienvenido/a ***********");
            System.out.println("   Acceso a la aplicacion");
            System.out.println("*********************************");
            System.out.println("1. Menu cliente");
            System.out.println("2. Menu mecanico");
            System.out.println("3. Menu gerente");
            System.out.println("---------------------------");
            System.out.println("4. Salir programa");
            System.out.println("---------------------------");

            System.out.print("Elegir Opcion: ");
            op = UtilidadesLP.leerCaracter();
            switch (op) 
            {
                case '1':
                    vo_mostrarMenuCliente();
                    break;
                case '2':
                    vo_mostrarMenuMecanico();
                    break;
                case '3':
                    vo_mostrarMenuGerente();
                    break;
                case '4':
                    System.out.println("FIN");
                    break;
                default:
                    System.out.println("******** ERROR ***********");
                    System.out.println("No es una opcion correcta!");
                    System.out.println("**************************");
            }
        } while (op != '4');
    }

    /**
    * Metodo privado que muestra el menu para el usuario tipo gerente.
    * Permite acceder a funcionalidades especificas para la gestion 
    * administrativa del taller mecanico.
    */
    private void vo_mostrarMenuGerente() 
    {

        String str_usuario;
        String str_clave;

        System.out.println("Teclea usuario gerente:");
        str_usuario = UtilidadesLP.leerCadena();
        System.out.println("Teclea clave de acceso:");
        str_clave = UtilidadesLP.leerCadena();

        if (str_usuario.equals("admin") && str_clave.equals("admin"))
            vo_mostrarMenuAdministrador();
        else
            System.out.println("Credenciales incorrectas!");

    }

    /**
    * Metodo privado que muestra el menu para el usuario tipo administrador.
    * Ofrece opciones para anadir, borrar, modificar y visualizar clientes y 
    * vehiculos.
    */
    private void vo_mostrarMenuAdministrador() 
    {

        char op;
        do
        {
            System.out.println("-------------------------------");
            System.out.println("MENU ADMIN TALLER MECANICO");
            System.out.println("-------------------------------");
            System.out.println("1.Insertar cliente");
            System.out.println("2.Insertar coche");
            System.out.println("3.Borrar cliente");
            System.out.println("4.Borrar vehiculo");
            System.out.println("5.Modificar cliente");
            System.out.println("6.Modificar vehiculo");
            System.out.println("---------------------------");
            System.out.println("7. Salir programa");
            System.out.println("---------------------------");

            System.out.print("Elegir Opcion: ");
            op = UtilidadesLP.leerCaracter();
            switch (op) {
            
                case '1':
                    vo_menuAñadirCliente();
                    break;
                case '2':
                    vo_menuAñadirCoche();
                    break;
                case '3':
                    vo_menuBajaUsuario();
                    break;
                case '4':
                    vo_menuBajaVehiculo();
                    break;
                case '5':
                    vo_menuModificarUsuario();
                    break;
                case '6':
                    vo_menuModificarVehiculo();
                    break;
                case '7':
                    System.out.println("FIN");
                    break;
                default:
                    System.out.println("******** ERROR ***********");
                        System.out.println("No es una opcion correcta!");
                        System.out.println("**************************");
                }
            } while (op != '3');
      
    }

    /**
    * Metodo privado que muestra el menu para el usuario tipo mecanico.
    * Verifica si el usuario esta registrado y muestra un menu simplificado con
    * opciones especificas para mecanicos.
    */
    private void vo_mostrarMenuMecanico() 
    {

        String str_nombre;
        boolean bln_respuesta;

        System.out.println("Teclea nombre usuario registrado: ");
        str_nombre = UtilidadesLP.leerCadena();

        bln_respuesta = obj_gestorLN.bln_esUsuarioRegistrado(str_nombre);

        if (bln_respuesta) 
        {
            vo_mostrarMenuRegistrado();
        } else 
        {
            System.out.println("No estas registrado!");
            vo_pausa();
        }
    }

    /**
    * Metodo privado que muestra el menu para el usuario registrado (mecanico).
    * Proporciona opciones para anadir clientes y vehiculos desde el menu 
    * principal.
    */
    private void vo_mostrarMenuRegistrado() 
    {

        char op;
        do
        {
            System.out.println("-------------------------------");
            System.out.println("MENU MECANICO TALLER MECANICO");
            System.out.println("-------------------------------");
            System.out.println("1.Insertar cliente");
            System.out.println("2.Insertar vehiculo");
            System.out.println("---------------------------");
            System.out.println("3. Salir programa");
            System.out.println("---------------------------");

            System.out.print("Elegir Opcion: ");
            op = UtilidadesLP.leerCaracter();
            switch (op) 
            {
            
                case '1':
                    vo_menuAñadirCliente();
                    break;
                case '2':
                    vo_menuAñadirCoche();
                    break;
                case '3':
                    System.out.println("FIN");
                    break;
                default:
                    System.out.println("******** ERROR ***********");
                    System.out.println("No es una opcion correcta!");
                    System.out.println("**************************");
                }
        } while (op != '3');
    }

    /**
    * Metodo privado que muestra el menu para el usuario tipo cliente.
    * Permite visualizar diferentes listados de clientes y vehiculos.
    */
    private void vo_mostrarMenuCliente() 
    {
        char op = '@';
        do 
        {
            System.out.println("---------------------------");
            System.out.println("MENU CLIENTE TALLER MECANICO");
            System.out.println("---------------------------");
            System.out.println("1.Visualizar clientes");
            System.out.println("2.Visualizar clientes ordenados por direccion"
            );
            System.out.println("3.Visualizar coches");
            System.out.println("4.Visualizar coches ordenados por matricula");
            System.out.println("---------------------------");
            System.out.println("5.Salir menu principal");
            System.out.println("---------------------------");
            System.out.print("Opcion: ");

            op = UtilidadesLP.leerCaracter();

            switch (op) 
            {
                case '1':
                    vo_visualizarClientes();
                    break;
                case '2':
                    vo_visualizarClientesPorDireccion();
                    break;
                case '3':
                     vo_visualizarVehiculos();
                    break;
                case '4':
                    vo_visualizarCochesPorMatricula();
                    break;
                case '5':
                    break;
                default:
                    System.out.println("No es una opcion correcta!");
                    vo_pausa();
            }
        } while (op != '5');

    }


    /**
    * Metodo publico que muestra un menu para la gestion de usuarios.
    * Ofrece opciones para anadir, borrar, modificar y visualizar usuarios.
    */
    public void vo_menuAltaUsuario() 
    {
       
        char op;

        do 
        {
            op = UtilidadesLP.leerCaracter();

            switch (op) 
            {
                case 1:
                    vo_menuAñadirCliente();
                    break;
                case 2:
                    vo_menuBorrarCliente();
                    break;
                case 3:
                    vo_menuModificarUsuario();
                    break;
                case 4:
                    vo_menuVisualizarClientes();
                    break;
                case 5:
                    System.out.println("FIN");
                    break;
                default:
                    System.out.println("No es una opcion correcta!");
                    vo_pausa();
            }
        } while (op != 5);
    }

    /**
    * Metodo que muestra un mensaje de pausa en la consola y espera a que el
    * usuario presione INTRO para continuar.
    */
    private void vo_pausa() 
    {
        System.out.println("Pulsa <INTRO> para continuar...");
        UtilidadesLP.leerCadena();
    }

    /**
    * Metodo que permite borrar un cliente del sistema.
    * Recupera el nombre del cliente a eliminar y realiza la operacion con el
    * gestor correspondiente.
    */
    private void vo_menuBorrarCliente() 
    {
        boolean bln_r;
        String str_nombre;

        System.out.println("Teclea nombre jugador:");
        str_nombre = UtilidadesLP.leerCadena();

        bln_r = obj_gestorLN.vo_eliminarUsuario(str_nombre);

        if (bln_r) 
        {
            System.out.println("*****************************************");
            System.out.println("Usuario borrado del sistema con exito!");
            System.out.println("*****************************************");
            vo_pausa();
        } else 
        {
            System.out.println("******************************************");
            System.out.println("Ups,no hemos podido encontrar este usuario!");
            System.out.println("******************************************");
            vo_pausa();
        }
    }

    /**
    * Metodo que muestra la informacion de todos los clientes registrados en el
    * sistema.
    * Recupera la informacion de los clientes a traves del gestor y la muestra
    * en la consola.
    */
    private void vo_menuVisualizarClientes() 
    {
        ArrayList<String> obj_clientes = obj_gestorLN.
        arrl_recuperarInformacionUsuarios();

        if (obj_clientes.isEmpty()) 
        {
            System.out.println("-----------------------------");
            System.out.println("No hay clientes para mostrar");
            System.out.println("-----------------------------");
            vo_pausa();
        } else 
        {
            System.out.println("INFORMACION DE CLIENTES:");
            System.out.println("---------------------------");
            for (String str_cliente : obj_clientes) {

                System.out.println(str_cliente);
                System.out.println("----------------------");
            }
            vo_pausa();
        }
    }

    /**
    * Metodo que permite anadir un nuevo cliente al sistema.
    * Solicita los datos necesarios al usuario y realiza la operacion 
    * correspondiente con el gestor.
    */
    private void vo_menuAñadirCliente() 
    {
        String str_nombre;
        int Str_dni;
        String str_email;

        System.out.println("Teclea nombre del cliente:");
        str_nombre = UtilidadesLP.leerCadena();

        System.out.println("Teclea DNI del cliente:");
        Str_dni = UtilidadesLP.leerEntero();

        System.out.println("Teclea email del cliente:");
        str_email = UtilidadesLP.leerCadena();

        itfProperty bln_resultado = 
        obj_gestorLN.nuevo_Usuario(null,
        null,null,null,0,0);

        if (bln_resultado != null ) 
        {
            System.out.println("*****************************************");
            System.out.println("Excelente! Usuario añadido al sistema!");
            System.out.println("*****************************************");
            vo_pausa();
        } else 
        {
            System.out.println("*****************************************");
            System.out.println("Error! Usuario ya existe en el sistema!");
            System.out.println("*****************************************");
            vo_pausa();
        }
    }

     /**
    * Metodo publico que permite modificar un usuario existente.
    * Utiliza el objeto `obj_gestorLN` para realizar la modificacion.
    */
    public void vo_menuModificarUsuario() 
    {

        String str_dni;
        String str_nuevoNombre;
        String str_nuevoApellido;
        String str_nuevoDni;

        ArrayList<itfProperty> arrl_usuarios = obj_gestorLN.arrl_listaUsuarios();

        if (arrl_usuarios.isEmpty()) 
        {
            System.out.println("No hay usuarios para modificar.");
            return;
        }

        System.out.println("Introduce el DNI del usuario a modificar:");
        str_dni = UtilidadesLP.leerCadena();

        boolean bln_usuarioExiste = false;
        
        for (itfProperty obj_auxUsuario : arrl_usuarios) 
        {
            
            if (obj_auxUsuario.getObjectProperty(clsConstantes.DNI_USUARIO).
            equals(str_dni)) 
            {
                bln_usuarioExiste = true;
                break;
            }
        }

        if (bln_usuarioExiste) 
        {
            System.out.println("Introduce el nuevo nombre:");
            str_nuevoNombre = UtilidadesLP.leerCadena();

            System.out.println("Introduce el nuevo apellido:");
            str_nuevoApellido = UtilidadesLP.leerCadena();

            System.out.println("Introduce el nuevo DNI:");
            str_nuevoDni = UtilidadesLP.leerCadena();

            obj_gestorLN.vo_modificarUsuario(str_nuevoNombre, str_nuevoApellido,
                    str_nuevoDni);
        } else 
        {
            System.out.println("No hay usuarios para modificar.");
        }
    }

    /**
     * 
     */
    private void vo_menuModificarVehiculo() 
    {
        
    }

    /**
    * Metodo que permite dar de baja un vehiculo del sistema.
    * Muestra los vehiculos disponibles y solicita la matricula del vehiculo a
    * borrar.
    */
    private void vo_menuBajaVehiculo() 
    {

        vo_visualizarVehiculos();

        System.out.println("Dime la matricula del vehiculo a borrar: ");
        String mat = UtilidadesLP.leerCadena();
        obj_gestorLN.bln_borrarVehiculo( mat );

    }

    /**
    * Metodo que muestra la informacion de todos los coches ordenados por
    * matricula.
    * Recupera la informacion de los coches a traves del gestor y la muestra en
    * la consola.
    */
    private void vo_visualizarCochesPorMatricula() 
    {
     
        ArrayList<itfProperty> coches = obj_gestorLN.
        arrl_obtenerVehiculosPorMatricula();
        System.out.println("LOS COCHES ORDENADOS POR MATRICULA SON: ");
        for( itfProperty c : coches )
        {
            System.out.println( c.getObjectProperty(clsConstantes.MATRICULA) );
            System.out.println( c.getObjectProperty(clsConstantes.MODELO) );
        }

    }

    /**
    * Metodo que muestra la informacion de todos los vehiculos disponibles en el
    * sistema.
    * Recupera la informacion de los vehiculos a traves del gestor y la muestra
    * en la consola.
    */
    private void vo_visualizarVehiculos() 
    {
        
        ArrayList<itfProperty> vehiculos = obj_gestorLN.arrl_obtenerVehiculos();

        System.out.println("LOS VEHICULOS DISPONIBLES SON:");
        for( itfProperty v : vehiculos )
        {
            System.out.println( v.getObjectProperty(clsConstantes.MATRICULA));
            System.out.println( v.getObjectProperty(clsConstantes.COLOR));
            System.out.println( v.getObjectProperty(clsConstantes.MODELO));
            System.out.println( v.getObjectProperty(clsConstantes.KILOMETRAJE));
            System.out.println( v.getObjectProperty(clsConstantes.MOTOR));
        }

    }

    /**
    * Metodo que muestra la informacion de todos los clientes ordenados por
    * direccion.
    * Recupera la informacion de los clientes a traves del gestor y la muestra
    * en la consola.
    */
    private void vo_visualizarClientesPorDireccion() 
    {
        
        ArrayList<itfProperty> clientes = obj_gestorLN.
        arrl_obtenerClientesPorDireccion();
        for( itfProperty u : clientes )
        {
            System.out.println( u.getObjectProperty(clsConstantes.DNI_USUARIO));
            System.out.println( u.getObjectProperty(clsConstantes.DIRECCION) );
        }

    }

    /**
    * Metodo que muestra la informacion detallada de todos los clientes 
    * registrados en el sistema.
    * Recupera la informacion de los clientes a traves del gestor y la muestra
    * en la consola.
    */
    private void vo_visualizarClientes() 
    {
        
    
        ArrayList<itfProperty> usuarios = obj_gestorLN.arrl_recuperarUsuarios();

        for( itfProperty u : usuarios )
        {
            System.out.println(u.getObjectProperty(clsConstantes.DNI_USUARIO) );
            System.out.println(u.getObjectProperty(clsConstantes.
            APELLIDOS_USUARIO) );
            System.out.println(u.getObjectProperty(clsConstantes.NOMBRE_USUARIO)
            );
            
            if( u.getObjectProperty(clsConstantes.NUM_VISA) != null ) 
            {
                System.out.println("Es un cliente, tiene VISA y DIRECCION");
                System.out.println(u.getObjectProperty(clsConstantes.NUM_VISA));
                System.out.println(u.getObjectProperty(clsConstantes.DIRECCION));
            }
            if( u.getObjectProperty(clsConstantes.PUESTO) != null ) 
            {
                System.out.println("Es un mecanico, su puesto es:");
                System.out.println( u.getObjectProperty(clsConstantes.PUESTO ));
                System.out.println("Es un mecanico, su sueldo es:");
                System.out.println( u.getObjectProperty(clsConstantes.SUELDO ));                
            }
            if( u.getObjectProperty(clsConstantes.BENEFICIO) != null ) 
            {
                System.out.println("Es un Gerente, su beneficio es:");
                System.out.println(u.getObjectProperty(clsConstantes.BENEFICIO));
            }
        }
    
    }

    /**
    * Metodo que permite dar de baja a un usuario del sistema.
    * Muestra los clientes disponibles y solicita el DNI del cliente a borrar.
    */
    private void vo_menuBajaUsuario() {
        
        System.out.println("Estos son los clientes: ");
        vo_visualizarClientes();

        System.out.println("Teclea dni del cliente a borrar: ");
        String dni = UtilidadesLP.leerCadena();

        obj_gestorLN.bln_borrarUsuario( dni );
    }

     /**
    * Metodo privado que muestra un menu para anadir un nuevo vehiculo.
    * Recolecta informacion del vehiculo y utiliza el objeto `obj_gestorLN` para
    * anadirlo.
    */
    private void vo_menuAñadirCoche() 
    {
        System.out.println("Teclea color: ");
        String color = UtilidadesLP.leerCadena();
        System.out.println("Teclea matricula: ");
        String matricula = UtilidadesLP.leerCadena();
        System.out.println("Teclea modelo: ");
        String modelo = UtilidadesLP.leerCadena();
        System.out.println("Teclea kilometros: ");
        int kms = UtilidadesLP.leerEntero();
        System.out.println("Teclea motor: ");
        String motor = UtilidadesLP.leerCadena();
        System.out.println("Teclea numero de puertas: ");
        int puertas = UtilidadesLP.leerEntero();
        System.out.println("Teclea asientos: ");
        int asientos = UtilidadesLP.leerEntero();
        System.out.println("Teclea tipo de cambio: ");
        String tipoCambio = UtilidadesLP.leerCadena();
        
        obj_gestorLN.vo_insertarCoche(color, matricula, modelo, kms, 
        motor, puertas, asientos, tipoCambio );
        
    }

}

