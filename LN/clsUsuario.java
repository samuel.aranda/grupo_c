package LN;

import COMUN.*;

/**
 * Clase que representa un usuario. Permite gestionar los datos de un usuario,
 * como el nombre, apellidos, DNI, correo electronico y telefono.
 * Ademas permite comparar los usuarios entre ellos.
 * Implementa la interfaz itfProperty y es comparable.
 * 
 * @author u.garay
 * @version 3.0
 */
public class clsUsuario implements itfProperty,  Comparable<clsUsuario> {
    
    /**
     * Metodo para obtener una propiedad del objeto usuario.
     * 
     * @author u.garay
     * 
     * @param propiedad La propiedad que se desea obtener.
     * @return El valor de la propiedad.
     */
    @Override
    public Object getObjectProperty(String propiedad) 
    {
        
        switch( propiedad )
        {
            case clsConstantes.NOMBRE_USUARIO : return str_nombre;
            case clsConstantes.APELLIDOS_USUARIO : return str_apellidos;
            case clsConstantes.DNI_USUARIO : return str_dni;
            case clsConstantes.EMAIL_USUARIO : return str_email;
            case clsConstantes.TELEFONO_USUARIO : return int_telefono;

            default: return null;
        }        
    }

    /**
     * Metodo para calcular el hash del usuario.
     * 
     * @author u.garay
     * 
     * @return El hash del usuario.
     */
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((str_dni == null) ? 0 : str_dni.hashCode());
        return result;
    }

    /**
     * Metodo para comparar si dos usuarios son iguales.
     * 
     * @author u.garay
     * 
     * @param obj El objeto/usuario con el que se compara.
     * @return True si los usuarios son iguales, false en caso contrario.
     */
    @Override
    public boolean equals(Object obj) 
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
    
        if (getClass() != obj.getClass())
            return false;
        
        clsUsuario other = (clsUsuario) obj;
        if (str_dni == null) {
            if (other.str_dni != null)
                return false;
        } else if (!str_dni.equals(other.str_dni))
            return false;
        return true;
    }

    /**
     * Metodo que compara dos usuarios.
     * 
     * @author u.garay 
     * 
     * @param u El usuario con el que se compara.
     * @return Un numero negativo si este usuario es menor que el usuario u,
     * cero si son iguales, o un numero positivo si este usuario es mayor.
     */
    @Override
    public int compareTo( clsUsuario u )
    {
        return this.str_dni.compareTo(u.str_dni);
    }

    /**
     * Atributos de la clase clsUsuario.
     * 
     * @author u.garay
     */
    private String str_nombre;
    private String str_apellidos;
    private String str_dni;
    private String str_email;
    private int int_telefono;

    private int int_rol;

    /**
     * Constructor de la clase clsUsuario.
     * 
     * @author u.garay
     * 
     * Inicializa el usuario con los valores predeterminados.
     */
    public clsUsuario() 
    {
        this.str_nombre = "";
        this.str_apellidos = "";
        this.str_dni = "";
        this.str_email = "";
        this.int_telefono = 0;
    }

    /**
     * Constructor de la clase clsUsuario.
     * 
     * @author u.garay

     * @param str_nombre Nombre del usuario.
     * @param str_apellido Apellidos del usuario.
     * @param str_dni DNI del usuario.
     * @param str_email Correo electronico del usuario.
     * @param int_telefono Numero de telefono del usuario.
     * @param int_rol El rol del usuario
     */
    public clsUsuario(String str_nombre, String str_apellido, String str_dni, 
    String str_email, int int_telefono, int int_rol ) 
    {
        this.str_nombre = str_nombre;
        this.str_apellidos = str_apellido;
        this.str_dni = str_dni;
        this.str_email = str_email;
        this.int_telefono = int_telefono;
        this.int_rol = int_rol;
    }

    /**
     * Metodo para obtener el nombre del usuario.
     * 
     * @author u.garay
     * 
     * @return El nombre del usuario.
     */
    public String getNombre() 
    {
        return this.str_nombre;
    }

    /**
     * Getter del rol
     * 
     * @author u.garay
     * 
     * @return Numero del rol
     */
    public int getRol()
    {
        return int_rol;
    }

    /**
     * Metodo para establecer el nombre del usuario.
     * 
     * @author u.garay
     * 
     * @param str_nombre El nuevo nombre del usuario.
     */
    public void setNombre(String str_nombre) 
    {
        this.str_nombre = str_nombre;
    }

    /**
     * Metodo para obtener los apellidos del usuario.
     * 
     * @author u.garay
     * 
     * @return Los apellidos del usuario.
     */
    public String getApellidos() 
    {
        return this.str_apellidos;
    }

    /**
     * Metodo para establecer los apellidos del usuario.
     * 
     * @author u.garay
     * 
     * @param str_apellidos Los nuevos apellidos del usuario.
     */
    public void setApellidos(String str_apellidos) 
    {
        this.str_apellidos = str_apellidos;
    }

    /**
     * Metodo para obtener el DNI del usuario.
     * 
     * @author u.garay
     * 
     * @return El DNI del usuario.
     */
    public String getDni() 
    {
        return this.str_dni;
    }

    /**
     * Metodo para establecer el DNI del usuario.
     * 
     * @author u.garay
     * 
     * @param str_dni El nuevo DNI del usuario.
     */
    public void setDni(String str_dni) 
    {
        this.str_dni = str_dni;
    }

    /**
     * Metodo para obtener el correo electronico del usuario.
     * 
     * @author u.garay
     * 
     * @return El correo electronico del usuario.
     */
    public String getEmail() 
    {
        return this.str_email;
    }

    /**
     * Metodo para establecer el correo electronico del usuario.
     * 
     * @author u.garay
     * 
     * @param str_email El nuevo correo electronico del usuario.
     */
    public void setEmail(String str_email) 
    {
        this.str_email = str_email;
    }

    /**
     * Metodo para obtener el telefono del usuario.
     * 
     * @author u.garay
     * 
     * @return El telefono del usuario.
     */
    public int getTelefono() 
    {
        return this.int_telefono;
    }

    /**
     * Metodo para establecer el telefono del usuario.
     * 
     * @author u.garay
     * 
     * @param int_telefono El nuevo telefono del usuario.
     */
    public void setTelefono(int int_telefono) 
    {
        this.int_telefono = int_telefono;
    }

    /**
     * Metodo toString para representar el objeto como un string.
     * 
     * @author u.garay
     * 
     * @return El objeto como string.
     */
    public String toString() 
    {
        String retorno = "";
        retorno = this.str_nombre;
        retorno = retorno + " " + this.str_apellidos;
        retorno = retorno + " " + this.str_dni;
        retorno = retorno + " " + this.str_email;
        retorno = retorno + " " + this.int_telefono;

        return retorno;
    }
}
