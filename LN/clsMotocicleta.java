package LN;

import COMUN.clsConstantes;

/**
 * Clase que representa una motocicleta.
 * Es hija de la clase clsVehículo.
 * 
 * @author izaro.ortega
 * @version 3.0
 * 
 */
public class clsMotocicleta extends clsVehiculo 
{
    /**
     * Obtiene el valor de una propiedad de la motocicleta.
     * 
     * @param propiedad La propiedad cuyo valor se quiere obtener.
     * @return El valor de la propiedad especificada.
     * 
     * @author izaro.ortega
     */
    @Override 
    public Object getObjectProperty(String propiedad )
    {
        switch (propiedad) {
            
            case clsConstantes.NUMERO_RUEDAS:  
            return str_numeroRuedas;

            case clsConstantes.CILINDRADA:
             return str_cilindrada;

            default: return super.getObjectProperty(propiedad);
        }
    }


    /**
     * El numero de ruedas de la motocicleta.
     * 
     * @author izaro.ortega
     */
    private int str_numeroRuedas;

    /**
     * La cilindrada de la motocicleta.
     * 
     * @author izaro.ortega
     */
    private int str_cilindrada;

    /**
     * Constructor para crear una instancia de clsMotocicleta
     * con valores iniciales.
     * 
     * @author izaro.ortega
     */
    public clsMotocicleta() 
    {
        this.str_numeroRuedas = 0;
        this.str_cilindrada = 0;

    }

    /**
     * Constructor para crear una instancia de clsMotocicleta con
     * valores iniciales de numero de ruedas y ciclindrada.
     * 
     * @param str_numeroRuedas
     * @param str_cilindrada
     * 
     * @author izaro.ortega
     */
    public clsMotocicleta(int str_numeroRuedas, int str_cilindrada)
     {
        this.str_numeroRuedas = str_numeroRuedas;
        this.str_cilindrada = str_cilindrada;

    }

    /**
     * Obtiene el numero de ruedas de la motocicleta.
     * 
     * @return El numero de ruedas.
     * 
     * @author izaro.ortega
     */
    public int getNumeroruedas()
    {
        return this.str_numeroRuedas;
    }

    /**
     * Establece el numero de ruedas de la motocicleta.
     * 
     * @param str_numeroRuedas
     * 
     * @author izaro.ortega
     */
    public void setNumeroruedas(int str_numeroRuedas) 
    {
        this.str_numeroRuedas = str_numeroRuedas;
    }

    /**
     * Obtiene la cilindrada de la motocicleta.
     * 
     * @return La cilindrada.
     * 
     * @author izaro.ortega
     */
    public int getCilindrada() 
    {
        return this.str_cilindrada;
    }

    /**
     * Establece la cilindrada la motocicleta.
     * 
     * @param str_cilindrada
     * 
     * @author izaro.ortega
     */
    public void setCilindrada(int str_cilindrada) 
    {
        this.str_cilindrada = str_cilindrada;
    }

}