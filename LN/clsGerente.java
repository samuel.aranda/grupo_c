package LN;

import COMUN.clsConstantes;

/**
 * La clase clsGerente representa a un gerente que hereda de la
 * clase clsUsuario.
 * 
 * @author izaro.ortega
 * @version 3.0
 */
public class clsGerente extends clsUsuario 
{

    /**
     * Obtiene el valor de un propiedad del gerente.
     * 
     * @param propiedad La propiedad cuyo valor se quiere obtener.
     * @return El valor de la propiedad especificada.
     * 
     * @author izaro.ortega
     */
    @Override
    public Object getObjectProperty(String propiedad) 
    {
        switch (propiedad) {

            case clsConstantes.BENEFICIO:
                return str_beneficio;

            case clsConstantes.INVERSION:
                return str_inversion;

            case clsConstantes.PROPIEDADES:
                return str_propiedades;

            default:
                return super.getObjectProperty(propiedad);
        }
    }

    /**
     * El beneficio del gerente
     * 
     * @author izaro.ortega
     * 
     */
    private double str_beneficio;
    /**
     * La inversion del gerente
     * 
     * @author izaro.ortega
     */
    private double str_inversion;
    /**
     * Las propiedades del gerente
     * 
     * @author izaro.ortega
     */
    private double str_propiedades;

    /**
     * Constructor por defecto para crear una instancia de clsGerente con 
     * valores iniciales.
     * 
     * @author izaro.ortega
     */

    public clsGerente() 
    {
        this.str_beneficio = 0;
        this.str_inversion = 0;
        this.str_propiedades = 0;
    }

    /**
     * Constructor para crear una instancia de clsGerente con valores
     * iniciales de beneficios, inversión y propiedades.
     * 
     * @param str_beneficio   El beneficio inicial del gerente.
     * @param str_inversion   La inversión inicial del gerente.
     * @param str_propiedades Las propiedades iniciales del gerente.
     * 
     * @author izaro.ortega
     */

    public clsGerente(String str_nombre, String str_apellido,
            String str_dni, String str_email, int int_telefono, int int_rol, double str_beneficio, double str_inversion,
            double str_propiedades) 
    {

        super(str_nombre, str_apellido, str_dni, str_email, int_telefono, int_rol);
        this.str_beneficio = str_beneficio;
        this.str_inversion = str_inversion;
        this.str_propiedades = str_propiedades;
    }

    /**
     * Obtiene los beneficios del gerente.
     *
     * @return Los beneficios del gerente.
     * 
     * @author izaro.ortega
     */

    public double getBeneficios() 
    {
        return this.str_beneficio;
    }

    /**
     * Establece los bebficios del gerente
     * 
     * @param str_beneficio El nuevo beneficio del gerente
     * 
     * @author izaro.ortega
     */

    public void setBeneficios(double str_beneficio) 
    {
        this.str_beneficio = str_beneficio;
    }

    /**
     * Obtiene la inversion del gerente.
     * 
     * @return La inversion del gerente.
     * 
     * @author izaro.ortega
     */

    public double getInversion() 
    {
        return this.str_inversion;
    }

    /**
     * Establece la inversion del gerente.
     * 
     * @param str_inversion La nueva inversion del gerente.
     * 
     * @author izaro.ortega
     */

    public void setInversion(double str_inversion)
    {
        this.str_inversion = str_inversion;
    }

    /**
     * Obtiene las propiedades del gerente.
     * 
     * @return Las propiedades del gerente.
     * 
     * @author izaro.ortega
     */

    public double getPropiedades() 
    {
        return this.str_propiedades;
    }

    /**
     * Establece las propiedades del gerente.
     * 
     * @param str_propiedades Las nuevas propiedades del gerente.
     * 
     * @author izaro.ortega
     */

    public void setPropiedades(double str_propiedades) 
    {
        this.str_propiedades = str_propiedades;
    }

}
