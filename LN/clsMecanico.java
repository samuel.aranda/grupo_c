package LN;

import COMUN.clsConstantes;

/**
 * Clase que que contiene los atriburos de mecanico
 * 
 * @author izaro.ortega
 * @version 3.0
 */

public class clsMecanico extends clsUsuario 
{

    /**
     * Obtiene el valor de un propiedad del mecanico.
     * 
     * @param propiedad La propiedad cuyo valor se quiere obtener.
     * @return El valor de la propiedad especificada.
     * 
     * @author izaro.ortega
     */
    @Override
    public Object getObjectProperty(String propiedad) 
    {

        switch (propiedad) 
        {

            case clsConstantes.PUESTO:
                return str_puesto;

            case clsConstantes.SUELDO:
                return str_sueldo;

            default:
                return super.getObjectProperty(propiedad);
        }

    }

    /**
     * El puesto del mecanico.
     * 
     * @author izaro.ortega
     */
    private String str_puesto;

    /**
     * El sueldo del mecanico.
     * 
     * @author izaro.ortega
     */
    private String str_sueldo;

    /**
     * Constructor por defecto para crear una instancia de clsMecanico con
     * valores iniciales.
     * 
     * @author izaro.ortega
     */
    public clsMecanico() 
    {
        this.str_puesto = "";
        this.str_sueldo = "";

    }

    /**
     * Constructor para crear una instancia de clsGerente con valores
     * iniciales de puesto y sueldo.
     * 
     * @param str_puesto El puesto del mecanico.
     * @param str_sueldo El sueldo del mecanico.
     * 
     * @author izaro.ortega
     */
    public clsMecanico(String str_nombre, String str_apellido,
            String str_dni, String str_email, int int_telefono, int int_rol, String str_puesto, String str_sueldo) 
    {
        super(str_nombre, str_apellido, str_dni, str_email, int_telefono, int_rol);
        this.str_puesto = str_puesto;
        this.str_sueldo = str_sueldo;
    }

    /**
     * Obtiene el puesto del mecanico.
     * 
     * @return El puesto del mecanico.
     * 
     * @author izaro.ortega
     */
    public String getPuesto()
    {
        return this.str_puesto;
    }

    /**
     * Establece el puesto del mecanico.
     * 
     * @param str_puesto
     * 
     * @author izaro.ortega
     */
    public void setPuesto(String str_puesto) 
    {
        this.str_puesto = str_puesto;
    }

    /**
     * Obtiene el sueldo del mecanico.
     * 
     * @return El sueldo del mecanico.
     * 
     * @author izaro.ortega
     */
    public String getSueldo()
    {
        return this.str_sueldo;
    }

    /**
     * Establece el sueldo del mecanico.
     * 
     * @param str_sueldo
     * 
     * @author izaro.ortega
     */
    public void setSueldo(String str_sueldo) 
    {
        this.str_sueldo = str_sueldo;
    }

}
