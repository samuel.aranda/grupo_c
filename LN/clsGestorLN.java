package LN;

import java.util.*;

import COMUN.itfProperty;

/**
 * Clase que gestiona la lista de usuarios, permitiendo dar de alta,
 * dar de baja y modificar datos de los mismos.
 * 
 * Esta clase utiliza un conjunto para almacenar usuarios y un mapa para
 * almacenar vehiculos.
 * 
 * @author unai.pinilla
 * @version 3.00
 */
public class clsGestorLN 
{

    private Set<clsUsuario> usuarios;

    private Map<String , clsVehiculo> vehiculos;


    /**
     * Constructor de la clase clsGestorLN.
     * Inicializa la lista de usuarios y el mapa de vehiculos.
     * 
     * @author unai.pinilla
     */
    public clsGestorLN() 
    {

        usuarios = new TreeSet<>();  
        vehiculos = new HashMap<>(); 

        vo_inicializar();
    }

    /**
     * Metodo que inicializa la lista de usuarios y el mapa de vehiculos
     * con datos de ejemplo.
     */
    public void vo_inicializar() 
    {
        
        usuarios.add(new clsUsuario("admin",
        "admin", "admin", 
        "admin@admin.com", 0, 0));

        clsUsuario c = new clsCliente("Cliente1", 
        "Apellido1","DNI1", 
        "Email1",943000000, 
        1, "Direccion1" , "Visa1");
        usuarios.add( c );

        clsUsuario g = new clsGerente("Gerente",
        "Apellido Gerente", "DniG",
        "gerente@gmail.com",945111111, 
        2, 0, 0, 0);
        usuarios.add( g );

        clsUsuario m = new clsMecanico("Mecanico",
        "Apellido mecanico","11223344W",
        "mecanico@gmail.com", 666111555,
         4, "Mecanico","1000");

        usuarios.add( m );


        String m1 = "";
        clsVehiculo v1=null;
        vehiculos.put(m1, v1);

        String m2 = "";
        clsVehiculo v2=null;
        vehiculos.put(m2, v2);

        String m3 = "";
        clsVehiculo v3=null;
        vehiculos.put(m3, v3);

            
    }

    /**
     * Metodo que permite insertar un nuevo cliente en la lista de usuarios.
     * 
     * @param str_nombre    Nombre del cliente.
     * @param str_apellido  Apellido del cliente.
     * @param str_dni       DNI del cliente.
     * @param str_email     Email del cliente.
     * @param int_telefono  Numero de telefono del cliente.
     * @param int_rol       Rol del cliente.
     * @param str_direccion Direccion del cliente.
     * @param str_numVisa   Numero de Visa del cliente.
     */
    public void vo_insertarCliente(String str_nombre, String str_apellido, 
    String str_dni, String str_email, int int_telefono, int int_rol ,
    String str_direccion, String str_numVisa )
    {
        usuarios.add( new clsCliente(str_nombre, str_apellido, 
        str_dni,  str_email, int_telefono,  int_rol ,
         str_direccion,  str_numVisa) );
    }

    /**
     * Metodo que permite insertar un nuevo gerente en la lista de usuarios.
     * 
     * @param str_nombre     Nombre del gerente.
     * @param str_apellido   Apellido del gerente.
     * @param str_dni        DNI del gerente.
     * @param str_email      Email del gerente.
     * @param int_telefono   Numero de telefono del gerente.
     * @param int_rol        Rol del gerente.
     * @param str_beneficio  Beneficio del gerente.
     * @param str_inversion  Inversión del gerente.
     * @param str_propiedades Propiedades del gerente.
     */
    public void vo_insertarGerente(String str_nombre, String str_apellido, 
    String str_dni, String str_email, int int_telefono, int int_rol, 
    double str_beneficio, double str_inversion, double str_propiedades)
    {
        usuarios.add( new clsGerente(str_nombre, str_apellido, 
        str_dni,  str_email, int_telefono,  int_rol ,
         str_beneficio,  str_inversion, str_propiedades ) );
    }

    /**
     * Metodo que permite insertar un nuevo mecanico en la lista de usuarios.
     * 
     * @param str_nombre    Nombre del mecanico.
     * @param str_apellido  Apellido del mecanico.
     * @param str_dni       DNI del mecanico.
     * @param str_email     Email del mecanico.
     * @param int_telefono  Numero de telefono del mecanico.
     * @param int_rol       Rol del mecanico.
     * @param str_puesto    Puesto del mecanico.
     * @param str_sueldo    Sueldo del mecanico.
     */
    public void vo_insertarMecanico(String str_nombre, String str_apellido, 
    String str_dni, String str_email, int int_telefono, 
    int int_rol,String str_puesto, String str_sueldo) 
    {
        usuarios.add( new clsMecanico(str_nombre, str_apellido, 
        str_dni, str_email, int_telefono, int_rol, str_puesto, str_sueldo));
    }

    /**
     * Metodo que permite borrar un usuario de la lista segun su DNI.
     * 
     * @param str_param_dni DNI del usuario a borrar.
     * @return True si se elimino con exito, false en caso contrario.
     */
    public boolean bln_borrarUsuario(String str_param_dni )
    {
        Iterator<clsUsuario> it = usuarios.iterator();

        while( it.hasNext() )
        {
            clsUsuario u = it.next();
            if ( u.getDni().equals(str_param_dni ) )
            {
                it.remove();
                return true;
            }
        }

        return false;
    }

    /**
     * Metodo que busca un usuario en la lista segun su DNI.
     * 
     * @param str_param_dni DNI del usuario a buscar.
     * @return El usuario encontrado o null si no se encontro.
     */
    public clsUsuario buscarUsuario(String str_param_dni )
    {
        Iterator<clsUsuario> it = usuarios.iterator();

        while( it.hasNext() )
        {
            clsUsuario u = it.next();
            if ( u.getDni().equals(str_param_dni ) )
            {
                return u;
            }
        }

        return null;

    }

    /**
     * Metodo que permite insertar un nuevo coche en el mapa de vehiculos.
     * 
     * @param str_color         Color del coche.
     * @param str_matricula     Matricula del coche.
     * @param str_modelo        Modelo del coche.
     * @param int_kilometraje   Kilometraje del coche.
     * @param str_motor         Motor del coche.
     * @param str_numeroPuertas Numero de puertas del coche.
     * @param str_numeroAsientos Numero de asientos del coche.
     * @param str_tipoCambio    Tipo de cambio del coche.
     */
    public void vo_insertarCoche(String str_color, String str_matricula,
        String str_modelo, int int_kilometraje, String str_motor , 
        int str_numeroPuertas, int str_numeroAsientos,
        String str_tipoCambio)
    {
    
        clsCoche c = new clsCoche(str_color, str_matricula , 
        str_modelo , int_kilometraje , str_motor , str_numeroPuertas, 
        str_numeroAsientos, str_tipoCambio);
        
        vehiculos.put(str_matricula, c );
    }

    /**
     * Metodo que permite borrar un vehiculo del mapa segun su matricula.
     * 
     * @param matricula Matricula del vehículo a borrar.
     * @return True si se elimino con exito, false en caso contrario.
     */
    public boolean bln_borrarVehiculo(String matricula )
    {
        clsVehiculo v = vehiculos.remove(matricula);

        if ( v != null )
            return true;
        else
            return false;
    }

    /**
     * Metodo que busca un vehiculo en el mapa segun su matricula.
     * 
     * @param matricula Matricula del vehiculo a buscar.
     * @return El vehiculo encontrado o null si no se encontro.
     */
    public clsVehiculo buscarVehiculo(String matricula )
    {
        return vehiculos.get(matricula);
    }

    
    /**
     * Metodo que permite recuperar la lista de usuarios.
     * 
     * @return Lista de usuarios.
     */
    public ArrayList<itfProperty> arrl_recuperarUsuarios()
    {
        
        ArrayList<itfProperty> r = new ArrayList<>();

        r.addAll(usuarios);

        return r;
    }

     /**
     * Metodo que permite recuperar la lista de vehiculos ordenados por
     * matricula.
     * 
     * @return Lista de vehículos ordenados por matrícula
     */
    public ArrayList<itfProperty> arrl_recuperarVehiculosOrdenadosPorMatricula()
    {
        ArrayList<clsVehiculo> r = new ArrayList<>();
        
        r.addAll( vehiculos.values() ); 

        Collections.sort( r , new clsComparadorVehiculos() );


        ArrayList<itfProperty> datos = new ArrayList<>();
        datos.addAll(r);
        return datos;
    }


    /**
     * Metodo que crea un nuevo usuario y lo anade a la lista de usuarios.
     *
     * @author unai.pinilla
     * @param str_nombre   Nombre del usuario.
     * @param str_apellido Apellido del usuario.
     * @param str_dni      DNI del usuario.
     * @param str_email    Email del usuario.
     * @param int_telefono Numero de telefono del usuario.
     * @return El nuevo usuario creado.
     */
    public clsUsuario nuevo_Usuario(String str_nombre, String str_apellido,
            String str_dni, String str_email, int int_telefono, int int_rol) 
    {

        clsUsuario objP1;

        objP1 = new clsUsuario(str_nombre, str_apellido, str_dni, str_email,
                int_telefono, int_rol);

        usuarios.add(objP1);

        return objP1;
    }


    /**
     * Metodo que obtiene la lista de usuarios.
     * 
     * @author unai.pinilla
     * @return La lista de usuarios.
     */
    public ArrayList<itfProperty> arrl_listaUsuarios() 
    {
        
        ArrayList<itfProperty> arrl_resultado;

        arrl_resultado = new ArrayList<>();
        
        arrl_resultado.addAll( this.usuarios );
        
        return arrl_resultado;
    
    }


    /**
     * Metodo que elimina un usuario de la lista de usuarios segun su DNI.
     * 
     * @author unai.pinilla
     * @param str_dni DNI del usuario a eliminar.
     */
    public boolean vo_eliminarUsuario(String str_dni) 
    {
        clsUsuario obj_usuarioEliminado;

        obj_usuarioEliminado = null;
        for (clsUsuario obj_auxUsuario : this.usuarios) 
        {
            if (obj_auxUsuario.getDni().equals(str_dni)) 
            {
                obj_usuarioEliminado = obj_auxUsuario;
                this.usuarios.remove(obj_usuarioEliminado);
                return true;
            }
        }

        return false;

    }


    /**
     * Metodo que modifica los datos de un usuario segun el DNI que se pida.
     * 
     * @author unai.pinilla
     * @param str_dni      DNI del usuario a modificar.
     * @param str_nombre   Nuevo nombre del usuario.
     * @param str_apellido Nuevo apellido del usuario.
     * @param str_dni      Nuevo DNI del usuario.
     */
    public void vo_modificarUsuario(String str_nombre, String str_apellido,
            String str_dni) 
    {
        String str_nuevoNombre;
        String str_nuevoApellido;
        String str_nuevoDni;

        str_nuevoNombre = "";
        str_nuevoApellido = "";
        str_nuevoDni = "";

        for (clsUsuario obj_auxUsuario : this.usuarios) 
        {
            if (obj_auxUsuario.getDni().equals(str_dni)) 
            {
                obj_auxUsuario.setNombre(str_nuevoNombre);
                obj_auxUsuario.setApellidos(str_nuevoApellido);
                obj_auxUsuario.setDni(str_nuevoDni);
                return;
            }
        }
    }


    /**
     * Metodo que obtiene las credenciales de un usuario basado en su nombre.
     * 
     * @param str_usuario Nombre del usuario del cual se desean obtener las 
     * credenciales.
     * @return El rol del usuario si se encuentra, o 2 si no se encuentra.
     */
    public int int_obtenerCredenciales(String str_usuario) 
    {

        for (clsUsuario u : usuarios) 
        {
            if (u.getNombre().equals(str_usuario)) 
            {
                return u.getRol();
            }
        }

        return 2;
    }

    /**
     * Metodo que recupera la informacion de todos los usuarios en un ArrayList.
     * 
     * @return Un ArrayList con la informacion de todos los usuarios.
     */
    public ArrayList<String> arrl_recuperarInformacionUsuarios() 
    {

        ArrayList<String> infoUsuarios = new ArrayList<>();

        String info = "";

        // recorro cada jugador de la lista
        for (clsUsuario objUsuario : usuarios) {
            // añado a ese texto la informacion del jugador que quiero mostrar
            info = objUsuario.getNombre() + "-" + objUsuario.getApellidos() +
                    "-" + objUsuario.getRol();

            // esa info del jugador se añade a la respuesta que voy a enviar
            // al clsMenu
            infoUsuarios.add(info);
        }
        // le paso al clsMenu la informacion de todos
        return infoUsuarios;

    }

    /**
     * Metodo que verifica si un usuario esta registrado basado en su nombre.
     * 
     * @param str_nombre Nombre del usuario a verificar.
     * @return True si el usuario esta registrado, False en caso contrario.
     */
    public boolean bln_esUsuarioRegistrado(String str_nombre) {
     
        for( clsUsuario u : usuarios)
        {
            if( u.getNombre().equals( str_nombre ) )
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Metodo que obtiene una lista de clientes filtrados por direccion.
     * 
     * @return Un ArrayList con los clientes ordenados por direccion.
     */
    public ArrayList<itfProperty> arrl_obtenerClientesPorDireccion() {
        
        ArrayList<clsCliente> aux = new ArrayList<>();
        
        //FILTRAR TODOS LOS CLIENTES
        Iterator<clsUsuario> it = this.usuarios.iterator();
        while ( it.hasNext() )
        {
            clsUsuario u = it.next();
            if( u instanceof clsCliente )
            {
                clsCliente c = (clsCliente)u;
                aux.add( c );
            }
        }

        Collections.sort( aux , new clsComparadorClientes() );

        ArrayList<itfProperty> retorno = new ArrayList<>();
        retorno.addAll( aux );

        return retorno;

    }

     /**
     * Metodo que obtiene una lista de todos los vehiculos.
     * 
     * @return Un ArrayList con todos los vehiculos.
     */
    public ArrayList<itfProperty> arrl_obtenerVehiculos() 
    {
        
        ArrayList<itfProperty> resultado = new ArrayList<>();
        
        resultado.addAll( vehiculos.values() );
        
        return resultado;
    
    }

    /**
     * Metodo que obtiene una lista de vehiculos ordenados por matricula.
     * 
     * @return Un ArrayList con los vehiculos ordenados por matricula.
     */
    public ArrayList<itfProperty> arrl_obtenerVehiculosPorMatricula() 
    {

        ArrayList<clsVehiculo> aux = new ArrayList<>();
                
        aux.addAll( vehiculos.values() );

        Collections.sort( aux , new clsComparadorVehiculos() );

        ArrayList<itfProperty> retorno = new ArrayList<>();

        return retorno;
    }

}
