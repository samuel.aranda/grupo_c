package LN;

import COMUN.clsConstantes;

/**
 * Clase que representa un coche.
 * Es hija de la clase clsVehículo.
 * 
 * @author izaro.ortega
 * @version 3.0
 * 
 */
public class clsCoche extends clsVehiculo 
{
    /**
     * Obtiene el valor de una propiedad del coche.
     * 
     * @param propiedad La propiedad cuyo valor se quiere obtener.
     * @return El valor de la propiedad especificada.
     * 
     * @author izaro.ortega
     */
    @Override
    public Object getObjectProperty( String propiedad )
    {
        switch (propiedad) 
        {

            case clsConstantes.NUMERO_PUERTAS: 
             return str_numeroPuertas;      

            case clsConstantes.NUMERO_ASIENTOS: 
             return str_numeroAsientos;    

            case clsConstantes.TIPO_CAMBIO:
             return str_tipoCambio;   

            default:
                return super.getObjectProperty(propiedad);
        }
    }

    /**
     * El numero de puertas del coche.
     * 
     * @author izaro.ortega
     */
    private int str_numeroPuertas;

    /**
     * El numero de asientos del coche.
     * 
     * @author izaro.ortega
     */
    private int str_numeroAsientos;

    /**
     * El tipo de cambio para el coche.
     * 
     * @author izaro.ortega
     */
    private String str_tipoCambio;

    /**
     * Constructor para crear una instancia de clsCoche
     * con valores iniciales.
     * 
     * @author izaro.ortega
     */
    public clsCoche() 
    {
        this.str_numeroPuertas = 0;
        this.str_numeroAsientos = 0;
        this.str_tipoCambio = " ";
    }

    /**
     * Constructor para crear una instancia de clsCoche con
     * valores iniciales de numero de puertas, numero de
     * asientos y tipo de cambio.
     * 
     * @param str_numeroPuertas
     * @param str_numeroAsientos
     * @param str_tipoCambio
     * 
     * @author izaro.ortega
     */

    public clsCoche(String str_color, String str_matricula,
    String str_modelo, int int_kilometraje, String str_motor , int str_numeroPuertas, int str_numeroAsientos,
    String str_tipoCambio) 
    {
        super(str_color, str_matricula, str_modelo, int_kilometraje, str_motor);
        this.str_numeroPuertas = str_numeroPuertas;
        this.str_numeroAsientos = str_numeroAsientos;
        this.str_tipoCambio = str_tipoCambio;
    }

    /**
     * Obtiene el numero de puertas del coche.
     * 
     * @return El numero de puertas del coche.
     * 
     * @author izaro.ortega
     */
    public int getNumeropuertas() 
    {
        return this.str_numeroPuertas;
    }

    /**
     * Establece el numero de puertas del coche.
     * 
     * @param str_numeroPuertas
     * 
     * @author izaro.ortega
     */
    public void setNumeropuertas(int str_numeroPuertas) 
    {
        this.str_numeroPuertas = str_numeroPuertas;
    }

    /**
     * Obtiene el numero de asientos del coche.
     * 
     * @return El numero de asientos del coche.
     * 
     * @author izaro.ortega
     */
    public int getNumeroasientos() 
    {
        return this.str_numeroAsientos;
    }

    /**
     * Establece el numero de asientos del coche.
     * 
     * @param str_numeroAsientos
     * 
     * @author izaro.ortega
     */
    public void setNumeroasientos(int str_numeroAsientos)
    {
        this.str_numeroAsientos = str_numeroAsientos;
    }

    /**
     * Obtiene el tipo de cambio del coche.
     * 
     * @return El tipo de cambio del coche.
     * 
     * @author izaro.ortega
     */
    public String getTipocambio() 
    {
        return this.str_tipoCambio;
    }

    /**
     * Establece el tipo de cambiodel coche.
     * 
     * @param str_tipoCambio
     * 
     * @author izaro.ortega
     */
    public void setTipocambio(String str_tipoCambio) 
    {
        this.str_tipoCambio = str_tipoCambio;
    }

}
