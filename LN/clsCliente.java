package LN;

import COMUN.clsConstantes;

/**
 * Clase que representa un cliente.
 * Es hija de la clase clsUsuario e incluye informacion adicional como la
 * direccion y el numero de la tarjeta del cliente.
 * 
 * @author u.garay
 * @version 3.0
 */
public class clsCliente extends clsUsuario {

    /**
     * Metodo para obtener una propiedad del objeto cliente.
     * 
     * @author u.garay
     * 
     * @param propiedad La propiedad que se desea obtener.
     * @return El valor de la propiedad.
     */
    @Override
    public Object getObjectProperty(String propiedad)
    {
        switch (propiedad) {
            case clsConstantes.DIRECCION : return str_direccion;
            case clsConstantes.NUM_VISA : return str_numVisa;
            
            default:
                return super.getObjectProperty(propiedad);
        }
    }

    /**
     * Atributos de la clase clsCliente
     * 
     * @author u.garay
     */
    private String str_direccion;
    private String str_numVisa;

    /**
     * Constructor de la clase clsCliente.
     * Inicializa el cliente con los valores predeterminados.
     * 
     * @author u.garay
     */
    public clsCliente()
    {
        this.str_direccion = "";
        this.str_numVisa = "";
    }

    /**
     * Constructor de la clase clsCliente.
     * 
     * @author u.garay
     * 
     * @param str_direccion Direccion del cliente.
     * @param str_numVisa   Numero tarjeta del cliente.
     */
    public clsCliente(String str_nombre, String str_apellido, 
    String str_dni, String str_email, int int_telefono, int int_rol ,
    String str_direccion, String str_numVisa )
    {
        super( str_nombre, str_apellido,str_dni,str_email,int_telefono,int_rol);
        this.str_direccion = str_direccion;
        this.str_numVisa = str_numVisa;
    }

    /**
     * Metodo para obtener la direccion del cliente.
     * 
     * @author u.garay
     * 
     * @return La direccion del cliente.
     */
    public String getDireccion() 
    {
        return this.str_direccion;
    }

    /**
     * Metodo para establecer la direccion del cliente.
     * 
     * @author u.garay
     * 
     * @param str_direccion La nueva direccion del cliente.
     */
    public void setdireccion(String str_direccion) 
    {
        this.str_direccion = str_direccion;
    }

    /**
     * Metodo para obtener el numero de la tarjeta del cliente.
     * 
     * @author u.garay
     * 
     * @return El numero de la tarjeta del cliente.
     */
    public String getnumVisa() 
    {
        return this.str_numVisa;
    }

    /**
     * Metodo para establecer el numero de la tarjeta del cliente.
     * 
     * @author u.garay
     * 
     * @param str_numVisa El nuevo numero de la tarjeta del cliente.
     */
    public void setnumVisa(String str_numVisa) 
    {
        this.str_numVisa = str_numVisa;
    }

}

