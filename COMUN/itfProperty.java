package COMUN;

/**
 * Interfaz que permitira la comunicación entre las capas LP y LN. Define un
 * metodo que obtiene propiedades de objetos en LN. 
 * 
 * @author u.garay
 * 
 * @version 3.0
 */
public interface itfProperty 
{

    /**
     * Metodo para obtener una propiedad de un objeto.
     * 
     * @author u.garay
     * 
     * @param propiedad La propiedad que se desea obtener.
     * @return El valor de la propiedad.
     */
    public Object getObjectProperty( String propiedad );

}
