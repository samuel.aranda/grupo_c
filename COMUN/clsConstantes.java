package COMUN;

/**
 * Clase que define los atributos utilizados en el resto de clases del programa.
 * 
 * @author unai.pinilla
 * @version 3.0
 */
public class clsConstantes {

    /**
     * Atributos de la clase clsUsuario
     */
    public static final String NOMBRE_USUARIO = "Nombre del usuario";
    public static final String APELLIDOS_USUARIO = "Apellidos del usuario";
    public static final String DNI_USUARIO = "DNI del usuario";
    public static final String EMAIL_USUARIO = "Email del usuario";
    public static final String TELEFONO_USUARIO = "Telefono del usuario";
    
    
    /**
     * Atributos de la clase clsMecanico
     */
    public static final String PUESTO = "Puesto del mecanico";
    public static final String SUELDO = "Sueldo del mecanico";
    
    
    /**
     * Atributos de la clase clsGerente
     */
    public static final String BENEFICIO = "Beneficio del gerente";
    public static final String INVERSION = "Inversion del gerente";
    public static final String PROPIEDADES = "Propiedades del gerente";
    
    
    /**
     * Atributos de la clase clsCliente
     */
    public static final String DIRECCION = "Direccion del cliente";
    public static final String NUM_VISA = "Numero de VISA";
        
    
    /**
     * Atributos de la clase clsVehiculo
     */
    public static final String COLOR = "Color del coche";
    public static final String MATRICULA = "Matricula del coche";
    public static final String MODELO = "Modelo del coche";
    public static final String KILOMETRAJE = "Kilometro del coche";
    public static final String MOTOR = "Motor del coche";
    
    
    /**
     * Atributos de la clase clsCoche
     */
    public static final String NUMERO_PUERTAS = "Puertas del coche";
    public static final String NUMERO_ASIENTOS = "Asientos del coche";
    public static final String TIPO_CAMBIO = "Tipo de cambio del coche";

    
    /**
     * Atributos de la clase clsMotocicleta
     */
    public static final String NUMERO_RUEDAS = "Ruedas de la moto";
    public static final String CILINDRADA = "Cilindrada de la moto";
}
